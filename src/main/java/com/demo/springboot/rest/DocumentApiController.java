package com.demo.springboot.rest;

import com.demo.springboot.dto.ResultDto;
import com.demo.springboot.service.QuadraticFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DocumentApiController {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentApiController.class);

    private final QuadraticFunction quadraticFunction;


    public DocumentApiController(QuadraticFunction quadraticFunction) {
        this.quadraticFunction = quadraticFunction;
    }

    @GetMapping("/api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateFunction(@RequestParam Double a,
                                                       @RequestParam Double b,
                                                       @RequestParam Double c) {

        LOG.info("---- a: {}", a);
        LOG.info("---- b: {}", b);
        LOG.info("---- c: {}", c);

        return ResponseEntity.ok(quadraticFunction.calculateFunction(a, b, c));
    }

}
