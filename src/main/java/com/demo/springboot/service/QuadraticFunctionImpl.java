package com.demo.springboot.service;

import com.demo.springboot.dto.ResultDto;
import org.springframework.stereotype.Service;

@Service
public class QuadraticFunctionImpl implements QuadraticFunction {

    @Override
    public ResultDto calculateFunction(Double a, Double b, Double c){

        double delta = b * b - 4 * a * c;

        if(delta > 0.0){
            double x1 = (-b - Math.sqrt(delta))/(2*a);
            double x2 = (-b + Math.sqrt(delta))/(2*a);

            return new ResultDto(x1, x2);

        }else if(delta == 0.0){

            double x0 = (-b) / (2*a);
            return new ResultDto(x0, null);
        }

        return new ResultDto();
    }

}
